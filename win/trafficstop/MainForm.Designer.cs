namespace trafficstop
{
    partial class TrafficStop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrafficStop));
            this.applicationIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.aboutTrafficStopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prefsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acceptButton = new System.Windows.Forms.Button();
            this.thresholdText = new System.Windows.Forms.TextBox();
            this.thresholdLabel = new System.Windows.Forms.Label();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // applicationIcon
            // 
            this.applicationIcon.ContextMenuStrip = this.contextMenu;
            this.applicationIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("applicationIcon.Icon")));
            this.applicationIcon.Text = "TrafficStop";
            this.applicationIcon.Visible = true;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutTrafficStopToolStripMenuItem,
            this.prefsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.ShowImageMargin = false;
            this.contextMenu.Size = new System.Drawing.Size(146, 70);
            // 
            // aboutTrafficStopToolStripMenuItem
            // 
            this.aboutTrafficStopToolStripMenuItem.Name = "aboutTrafficStopToolStripMenuItem";
            this.aboutTrafficStopToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.aboutTrafficStopToolStripMenuItem.Text = "About TrafficStop";
            // 
            // prefsToolStripMenuItem
            // 
            this.prefsToolStripMenuItem.Name = "prefsToolStripMenuItem";
            this.prefsToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.prefsToolStripMenuItem.Text = "Preferences...";
            this.prefsToolStripMenuItem.Click += new System.EventHandler(this.prefsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // acceptButton
            // 
            this.acceptButton.Location = new System.Drawing.Point(180, 53);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(75, 23);
            this.acceptButton.TabIndex = 2;
            this.acceptButton.Text = "Accept";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
            // 
            // thresholdText
            // 
            this.thresholdText.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::trafficstop.Properties.Settings.Default, "thresholdSetting", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.thresholdText.Location = new System.Drawing.Point(126, 53);
            this.thresholdText.Name = "thresholdText";
            this.thresholdText.Size = new System.Drawing.Size(48, 20);
            this.thresholdText.TabIndex = 1;
            this.thresholdText.Text = global::trafficstop.Properties.Settings.Default.thresholdSetting;
            // 
            // thresholdLabel
            // 
            this.thresholdLabel.AutoSize = true;
            this.thresholdLabel.Location = new System.Drawing.Point(12, 56);
            this.thresholdLabel.Name = "thresholdLabel";
            this.thresholdLabel.Size = new System.Drawing.Size(108, 13);
            this.thresholdLabel.TabIndex = 3;
            this.thresholdLabel.Text = "Application threshold:";
            // 
            // TrafficStop
            // 
            this.AcceptButton = this.acceptButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.ControlBox = false;
            this.Controls.Add(this.thresholdLabel);
            this.Controls.Add(this.thresholdText);
            this.Controls.Add(this.acceptButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TrafficStop";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TrafficStop Preferences";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TrafficStop_FormClosing);
            this.Load += new System.EventHandler(this.TrafficStop_Load);
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon applicationIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem prefsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutTrafficStopToolStripMenuItem;
        private System.Windows.Forms.TextBox thresholdText;
        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.Label thresholdLabel;
    }
}

