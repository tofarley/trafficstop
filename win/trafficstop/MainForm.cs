using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using trafficstop.Properties;

namespace trafficstop
{
    public partial class TrafficStop : Form
    {
        public TrafficStop()
        {
            InitializeComponent();
        }

        private void prefsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // exits the application
            Application.Exit();
        }

        private void TrafficStop_Load(object sender, EventArgs e)
        {
            uint hUSB;
            int Result;
            StringBuilder DeviceName = new StringBuilder(Delcom.MAXDEVICENAMELEN);



               // this.thresholdText.Text = Settings.Default.windowCountThreshold.ToString();





            // Serach for the first match USB device, For USB IO Chips use USBIODS
            Result = Delcom.DelcomGetNthDevice(Delcom.USBIODS, 0, DeviceName);

            if (Result == 0)
            {  // if not found, exit
                MessageBox.Show("Where is the USB device, I can't find it!");
                return;
            }
            else
            {
                //MessageBox.Show("Device found: " + DeviceName);
            }

            hUSB = Delcom.DelcomOpenDevice(DeviceName, 0);                      // open the device
            Delcom.DelcomLEDControl(hUSB, Delcom.YELLOWLIGHT, Delcom.LEDON);    // blink the green led
            //Delcom.DelcomCloseDevice(hUSB);                                     // close the device
            MessageBox.Show("Press OK to turn the lights off.");
            Delcom.DelcomLEDControl(hUSB, Delcom.YELLOWLIGHT, Delcom.LEDOFF);
            Delcom.DelcomLEDControl(hUSB, Delcom.REDLIGHT, Delcom.LEDOFF);
            Delcom.DelcomLEDControl(hUSB, Delcom.GREENLIGHT, Delcom.LEDOFF);
            Delcom.DelcomCloseDevice(hUSB);
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void TrafficStop_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default.Save();
        }

    }
}