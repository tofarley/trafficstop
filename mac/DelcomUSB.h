/*
 *	DelcomBoard.h
 *
 *	Written by Steve Zellers, zellers@apple.com
 *
 *	Simple interface to the Delcom USB devices (802600, 802200) based
 *	on the Cypress CY763xxx chips.  To get debugging information, set
 *	the environment variable "DC_DEBUG" to 1 before running your test
 *	tool.
 *
 *	08/01/02	smz		created
 */

#ifndef __DELCOMUSB__
#define __DELCOMUSB__

#include <CoreServices/CoreServices.h>
#include <IOKit/usb/IOUSBLib.h>
#include <mach/mach.h>

#if __cplusplus
extern "C" {
#endif
	
/*
 * Types
 */

typedef struct DelcomBoard* DCBoardRef;

/*
 * Constants
 */

enum {
		eDCAPIVersionOne		= 0,		// API version

		eDC_VendorID			= 4037,		// idVendor (used internally)
		eDC_ProductID			= 4642,		// idProduct

		eDC_VisualIndicatorProductID = 0x1223,	// idProduct for the visual indicator
};

// Write firmware command
enum {
	kDCWriteCmd = 10,				// Major
	
	kDCWritePort1 = 2				// Minor
};

// Read firmware commands
enum {
	kDCReadCmd = 11,				// Major
	
	kDCReadBothPorts = 0			// Minor
};

extern UInt32 DCGetAPIVersion();

/*
 * Basic use.  Returns the 'nth' board found on the bus.  Doesn't
 * allow for hot-plugging of boards.  Opens the board and prepares it
 * for use.
 */
extern DCBoardRef DCFindBoard(int ixBoard);
extern void DCReleaseBoard(DCBoardRef ref);

/*
 * To find a specific variant of a delcom product
 */

extern DCBoardRef DCFindBoardByProductID(int ixBoard, UInt32 productID);

/*
 * Reset the board
 */
extern kern_return_t DCResetBoard(DCBoardRef ref);

/*
 * Low level write, corrosponding to the API Delcom publishes for
 * their firmware.  extraBytes may be NULL if extraLen is zero.
 */

extern kern_return_t DCControl(DCBoardRef ref, UInt8 majorCmd, UInt8 minorCmd, UInt8 msbData, UInt8 lsbData, UInt16 extraLen, void* extraBytes);

/*
 * Simple API; treats port 0 as 8 bits of input and port 1 as 8 bits
 * of output.  This is how the Delcom eval board is wired up.
 */
extern kern_return_t DCReadInputs(DCBoardRef ref, UInt8* port0);
extern kern_return_t DCWriteOutputs(DCBoardRef ref, UInt8 port1);

/*
 * Delcom Compatible API.  Once you've got the board reference, you
 * can use this API which is similar to what Delcom makes available
 * for Windows.  DCPacketControl takes a DCPacketStructure and optional
 * buffer parameter for longer reads and writes.
 */

enum {
	kDelcomUSBDeviceRecipient	=	8,
	kDelcomUSBDeviceModel		=	18
};

struct DCPacketStructure {
	UInt8 Recipient;
	UInt8 DeviceModel;
	UInt8 MajorCmd;
	UInt8 MinorCmd;
	UInt8 DataLSB;
	UInt8 DataMSB;
	UInt16 Length;
};

typedef struct DCPacketStructure DCPacketStructure; 

extern kern_return_t DCPacketControl(DCBoardRef ref, DCPacketStructure* ioStructure, void* ioBuffer);

/*
 * Low level access to IOKit fundamentals
 */

extern io_service_t DCGetDeviceService(DCBoardRef ref);
extern IOUSBDeviceInterface** DCGetDeviceInterface(DCBoardRef ref);


#if __cplusplus
}
#endif

#include <DelcomUSB/DelcomVisualIndicator.h>

#endif
