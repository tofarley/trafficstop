#import "AppController.h"

@implementation AppController
int appCount;
int threshold = 8;
DCBoardRef ref;

-(void)applicationLaunched: (NSNotification *) aNotification
{	
	appCount++;
	[self changeLight];
	NSLog(@"%d applications now running.", appCount);
}

-(void)applicationTerminated: (NSNotification *) aNotification
{
	appCount--;
	[self changeLight];
	NSLog(@"%d applications now running.", appCount);
}

- (void)setupMenuBarItem
{
    statusBar_menuBar = [NSStatusBar systemStatusBar];
	//statusItem_menuBarItem = [statusBar_menuBar statusItemWithLength:NSVariableStatusItemLength];
    statusItem_menuBarItem = [statusBar_menuBar statusItemWithLength:16];
	
    [statusItem_menuBarItem retain];
	//[statusItem_menuBarItem setImage:[NSImage imageNamed: @"green"]];
	[statusItem_menuBarItem setHighlightMode:YES];
	[statusItem_menuBarItem setMenu:menu_menuBar];
}

- (void)changeLight
{
	if(ref != nil)
	{
		if(appCount >= threshold)
		{
			DCWriteOutputs(ref, ~(1 << OFF));
			DCWriteOutputs(ref, ~(1 << RED));
			[statusItem_menuBarItem setImage:[NSImage imageNamed: @"red"]];
		}
		else if(appCount >= (threshold/2))
		{
			DCWriteOutputs(ref, ~(1 << OFF));
			DCWriteOutputs(ref, ~(1 << YELLOW));
			[statusItem_menuBarItem setImage:[NSImage imageNamed: @"yellow"]];
		}
		else
		{
			DCWriteOutputs(ref, ~(1 << OFF));
			DCWriteOutputs(ref, ~(1 << GREEN));
			[statusItem_menuBarItem setImage:[NSImage imageNamed: @"green"]];
		}
	}
}

- (void)turnOffLight
{
	DCWriteOutputs(ref, ~(1 << OFF));
	[statusItem_menuBarItem setImage:[NSImage imageNamed: @"off"]];	
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	// Determine the number of applications already running
	appCount = [[[NSWorkspace sharedWorkspace] launchedApplications] count];
	
	// Register to receive notifications when a new application is opened
	NSLog(@"%d applications running at launch time.", appCount);
	[[[NSWorkspace sharedWorkspace] notificationCenter]
		addObserver: self
		   selector: @selector(applicationLaunched:)
			   name: NSWorkspaceDidLaunchApplicationNotification
			 object: nil];
	
	// Register to receive notifications when an application is terminated
	[[[NSWorkspace sharedWorkspace] notificationCenter]
		addObserver: self
		   selector: @selector(applicationTerminated:)
			   name: NSWorkspaceDidTerminateApplicationNotification
			 object: nil];
	
	[self setupMenuBarItem];
	
	ref = DCFindBoard(0);
	
	if(ref == nil) {
		NSRunAlertPanel(@"USB Traffic Signal not found.",@"TrafficStop was unable to detect the USB traffic signal connected to your computer. Please check that the USB cable is properly connected and visit www.imagitronics.org for additional assistance.",@"OK",NULL,NULL);
		[NSApp terminate:self];
	}
	[self changeLight];
}

-(void)dealloc
{
	NSLog(@"Quitting! DEALLOC");
	[super dealloc];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
	if(ref != nil)
		[self turnOffLight];
	NSLog(@"Quitting! APPLICATIONWILLTERMINATE");
	[self dealloc];
}

@end
