//
//  main.m
//  TestCount
//
//  Created by Timothy Farley on 6/5/07.
//  Copyright __MyCompanyName__ 2007. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{

    return NSApplicationMain(argc,  (const char **) argv);

}
