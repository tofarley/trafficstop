/* AppController */

#import <Cocoa/Cocoa.h>
#import <CoreServices/CoreServices.h>
#import <IOKit/usb/IOUSBLib.h>
#import <mach/mach.h>
#import "DelcomUSB.h"

#define GREEN   1
#define YELLOW  2
#define RED     3
#define OFF     4

@interface AppController : NSObject
{
	IBOutlet NSMenu *menu_menuBar;
    NSStatusBar *statusBar_menuBar;
    NSStatusItem *statusItem_menuBarItem;

}
- (void)applicationLaunched:   (NSNotification *) aNotification;
- (void)applicationTerminated: (NSNotification *) aNotification;
- (void)setupMenuBarItem;
- (void)changeLight;
@end
